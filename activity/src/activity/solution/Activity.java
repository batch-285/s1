package activity.solution;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner myActivity = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = myActivity.nextLine();

        System.out.println("Last Name:");
        String lastName = myActivity.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = myActivity.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubject = myActivity.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubject = myActivity.nextDouble();

        double gradeAverage = (firstSubject + secondSubject + thirdSubject) / 3;
        int roundedGradeAverage = (int) Math.floor(gradeAverage);

        System.out.println("Good Day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + roundedGradeAverage);

    }
}
