package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        int age;
        char middleName;

        int x;
        int y = 1;

        x = 1;
        y = 2;

        int wholeNumber = 1000;
        System.out.println(wholeNumber);

        long worldPopulation = 1236547898523697L;
        System.out.println(worldPopulation);

        float piFloat = 1.236547898523697f;
        System.out.println(piFloat);

        double piDouble = 1.236547898523697;
        System.out.println(piDouble);

        char letter = 'c';
        System.out.println(letter);

        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        System.out.println("The value of y is " + y + " and the value of x is " + x);
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        String userName = "CardoD";
        System.out.println(userName);
        int stringLength = userName.length();
        System.out.println(stringLength);
    }
}
