package com.zuitt.example;
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter username:");

        String userName = myObj.nextLine();
        System.out.println("Username is:" + userName);

        //System.out.println("Enter a number to add:");
        System.out.println("Enter first number:");
        int num1 = Integer.parseInt(myObj.nextLine());
        int num2 = myObj.nextInt();
        System.out.println(num1+num2);

    }
}
